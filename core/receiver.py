import json
from datetime import timezone
import logging
import threading
from queue import SimpleQueue
from time import sleep

from ogn.parser import parse, ParseError
from ogn.client import AprsClient

from core.queue_ import feed_queue
from setting import BLACK_LIST


def feed_qba(red, debug):
    """
    Populate redis QBA queue (type list) from redis SQBA (type set)
    :param red: a redis pool client instance
    :param debug: boolean
    :return:
    """
    if debug:
        red.delete('QBA')
    while True:
        feed_queue(red, delete=not debug)
        if debug:
            break
        sleep(1)


def put_raw(raw_message, q):
    """
    AprsClient.run() method callback. Directly put raw_message in python simple queue
    and free up time for AprsClient.run() method
    :param raw_message:
    :param q: Python SimpleQueue
    :return:
    """
    q.put(raw_message)


def get_raw(q, red, check):
    while True:
        raw_message = q.get(block=True)
        try:
            # exclude FANET weather message before parsing (https://github.com/glidernet/ogn-aprs-protocol/issues/8)
            # exclude status messages before parsing
            matches = ('E_', 'W_', ':>')
            if any(m in raw_message for m in matches):
                continue

            # TODO check with new python-ogn-client release
            # beacon = parse(raw_message, reference_timestamp=datetime.now(timezone.utc))
            beacon = parse(raw_message)
            if beacon['aprs_type'] != 'position':
                continue
            if beacon['beacon_type'] not in ('aprs_aircraft', 'flarm', 'tracker', 'fanet'):
                # logging.debug("beacon_type not evaluate: {}".format(beacon["beacon_type"]))
                continue
            if 'address' not in beacon:
                continue
            address = beacon['address']
            if address in BLACK_LIST:
                continue
            ground_speed = beacon.get('ground_speed', None)
            if ground_speed is None:
                continue

            fix = dict()
            # TODO check with new python-ogn-client release
            # fix['tsp'] = int(datetime.timestamp(beacon['timestamp']))
            fix['tsp'] = int(beacon['timestamp'].replace(tzinfo=timezone.utc).timestamp())

            # multi-receivers case (height receiver density)=> we have to sort aircraft messages by timestamp
            # messages could be unordered when process, furthermore we can have 2 same records
            old_tsp = check.get(address, 0)
            if fix['tsp'] <= old_tsp:
                continue
            check[address] = fix['tsp']

            fix['alt'] = round(beacon['altitude']) if beacon['altitude'] is not None else None
            fix['lat'] = beacon['latitude']
            # lat is limited for areas very near to the poles
            # read: https://redis.io/commands/geoadd
            # exclude beacon message in this case
            if abs(beacon['latitude']) > 85.05:
                continue
            fix['lng'] = beacon['longitude']
            clr = beacon.get('climb_rate', None)
            fix['clr'] = round(clr, 2) if clr is not None else None
            fix['gsp'] = round(ground_speed)
            fix['track'] = beacon['track']
            fix['steal'] = beacon.get('stealth', False)
            fix['rname'] = beacon.get('receiver_name', None)

            # use redis pipeline for speed-up
            p = red.pipeline(transaction=False)     # python pipeline implementation use transactional by default
            p.rpush('BA:{}'.format(address), json.dumps(fix))
            p.geoadd('GEO:A', beacon['longitude'], beacon['latitude'], address)
            p.zadd('SSA', {address: fix['tsp']})
            p.sadd('SQBA', 'BA:{}'.format(address))
            p.execute()

        except ParseError as e:
            logging.debug(repr(e))
        except NotImplementedError as e:
            logging.debug("{}\nraw message: {}".format(repr(e), raw_message))
        except BaseException as e:
            logging.debug("{}\nraw message: {}".format(repr(e), raw_message))


def run(red, debug=False):
    client = AprsClient(aprs_user='NOCALL')
    client.connect()
    sq = SimpleQueue()
    check = dict()
    threading.Thread(target=get_raw, args=(sq, red, check), daemon=True).start()
    threading.Thread(target=feed_qba, args=(red, debug), daemon=True).start()
    try:
        client.run(callback=put_raw, autoreconnect=True, q=sq)
    except KeyboardInterrupt:
        print('\nStop ogn gateway')
        client.disconnect()
