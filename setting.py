# SETTING configuration
# DON'T USE ANY SECRET IN THIS FILE BUT USE setting_local.py INSTEAD

# REDIS SETTING
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0

# SQLITE SETTING
# if PATH is None, the sqlite file will be created on project top directory
SQLITE_PATH = None

# Aircraft paths are store in redis Stream
# a small PATH_SAMPLE & large PATH_CAPACITY increase path precision but need more RAM capacity
PATH_SAMPLE = 20            # PATH SAMPLE time in s
PATH_CAPACITY = 500         # nb max points
PATH_RETENTION = 86400      # in s

# PRIVACY MAPPING SALT
PRIVACY_SALT = "3v3ry 5tr1ng u w@nt h3r3"

# Black list beacon address in case of ...
BLACK_LIST = list()

# x-api-key
X_API_KEY = "CHANGEME"

# Try to override with setting_local if any
try:
    from setting_local import *
except ImportError:
    pass
